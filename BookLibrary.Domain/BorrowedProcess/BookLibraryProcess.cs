﻿using System;
using System.Collections.Generic;
using BookLibrary.Core;
using BookLibrary.Core.ServiceBus;
using BookLibrary.Events.BookLibraryProcess;

namespace BookLibrary.Domain.BorrowedProcess
{
    public partial class BookLibraryProcess:AggregateRoot<BookLibraryProcess>
    {
        [Obsolete("for serialization")]
        public BookLibraryProcess()
        {
            BookBorrowedRecords = new List<BorrowedRecord>();
            BookReturnedRecords = new List<ReturnedRecord>();
        }

        private BookLibraryProcess(Guid userId) : this()
        {
            UserId = userId;
            Id = Guid.NewGuid();
            BorrowedDate=DateTime.Now;

            EventRaiser.RaiseEvent(new BookLibraryProcessEvent.BookLibraryProcessCreatedEvent(bookLibraryProcessId:Id,userId:UserId,borrowedDate:BorrowedDate));
        }

        public Guid UserId { get; private set; }
        public DateTime BorrowedDate { get; private set; }
        public virtual List<BorrowedRecord> BookBorrowedRecords { get; private set; }
        public virtual List<ReturnedRecord> BookReturnedRecords { get; private set; }
    }
}
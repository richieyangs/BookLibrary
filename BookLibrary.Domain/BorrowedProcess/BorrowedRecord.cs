﻿using System;
using BookLibrary.Core;

namespace BookLibrary.Domain.BorrowedProcess
{
    public partial class BorrowedRecord:Entity
    {
        [Obsolete("for serialization")]
        public BorrowedRecord() { }

        public BorrowedRecord(BookLibraryProcess bookLibraryProcess, Guid userId, Book.Book book, TimeSpan borrowedInterval)
        {
            Id = Guid.NewGuid();

            BookLibraryProcess = bookLibraryProcess;
            UserId = userId;
            Book = book;
            BorrowedInterval = borrowedInterval;
            BorrowedDate=DateTime.Now;
        }
        public BookLibraryProcess BookLibraryProcess { get; private set; }
         public Guid UserId { get; private set; }
         public Book.Book Book { get; private set; }
         public DateTime BorrowedDate { get; private set; }
         public TimeSpan BorrowedInterval { get; private set; }
         public bool IsReturn { get; private set; }
    }
}
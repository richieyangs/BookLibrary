﻿using System;
using BookLibrary.Domain.Exceptions;

namespace BookLibrary.Domain.BorrowedProcess
{
    public partial class BorrowedRecord
    {
        public void Return()
        {
            if (IsReturn)
            {
                throw new DomainException($"Can not return this book:{Book.Id} since this book already has been returned");
            }
            IsReturn = true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookLibrary.QueryModel;
using BookLibrary.QueryModelReader.Contracts;

namespace BookLibrary.QueryModelReader.Implements
{
    public class BookLibraryProcessQueryReader : QueryReader, IBookLibraryProcessQueryReader
    {
        public BookLibraryProcessQueryReader(IQueryModelReaderSession session) : base(session)
        {
        }

        public BookLibraryProcessModel Get(Guid id)
        {
            return Session.Get<BookLibraryProcessModel,Guid>(id);
        }

        public BookLibraryProcessModel GetByUserId(Guid userId)
        {
            var models = Session.Get<BookLibraryProcessModel,Guid>(GetBookLibraryProcessId(userId).Value);
            return models;
        }

        public Guid? GetBookLibraryProcessId(Guid userId)
        {
            var index = UserAndLibraryProcessMapper.Index();
            var mappers = Session.GetAllItems<UserAndLibraryProcessMapper>(index);

            var processId = mappers.FirstOrDefault(x => x.UserId == userId)?.BookLibraryProcessId;
            return processId;
        }
    }
}
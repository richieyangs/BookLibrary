﻿using System;
using BookLibrary.Core.Events;

namespace BookLibrary.Events.User
{
    public partial class UserEvent
    {
        public class UserLoginedEvent : IEntityUpdatedEvent, IBookLibraryEvent
        {
            [Obsolete("for serilization")]
            public UserLoginedEvent()
            {
            }

            public UserLoginedEvent(Guid id, DateTime lastLoginDateTime)
            {
                Id = id;
                LastLoginDateTime = lastLoginDateTime;
            }

            public Guid Id { get; private set; }
            public DateTime LastLoginDateTime { get; private set; }
        }

    }
}
﻿using System;
using BookLibrary.Core.Events;

namespace BookLibrary.Events.BookLibraryProcess
{
    public partial class BookLibraryProcessEvent
    {
        public class BookLibraryProcessBookReturnedEvent : IEntityCreatedEvent, IBookLibraryEvent
        {
            [Obsolete("Only for Serilization")]
            public BookLibraryProcessBookReturnedEvent() { }

            public BookLibraryProcessBookReturnedEvent(Guid bookLibraryProcessId, ReturnedRecord returnedRecord)
            {
                BookLibraryProcessId = bookLibraryProcessId;
                ReturnedRecord = returnedRecord;
            }

            public Guid BookLibraryProcessId { get; private set; }
            public ReturnedRecord ReturnedRecord { get; private set; }
        }
    }
}
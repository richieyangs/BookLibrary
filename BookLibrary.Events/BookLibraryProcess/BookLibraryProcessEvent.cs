﻿using System;

namespace BookLibrary.Events.BookLibraryProcess
{
    public partial class BookLibraryProcessEvent
    {
        public class Book
        {
            [Obsolete("for serilization")]
            public Book()
            {
                
            }

            public Book(Guid bookId,string bookName)
            {
                BookId = bookId;
                BookName = bookName;
            }

            public Guid BookId { get; private set; }
            public string BookName { get; private set; }
        }

        public class BorrowedRecord
        {
            [Obsolete("for serilization")]
            public BorrowedRecord()
            {
            }

            public BorrowedRecord(Guid userId,Book book, DateTime borrowedDate,TimeSpan borrowedInterval)
            {
                UserId = userId;
                Book = book;
                BorrowedDate = borrowedDate;
                BorrowedInterval = borrowedInterval;
            }

            public Guid UserId { get; private set; }
            public Book Book { get; private set; }
            public DateTime BorrowedDate { get; private set; }
            public TimeSpan BorrowedInterval { get; private set; }
            public bool IsReturn { get; private set; }
        }

        public class ReturnedRecord
        {
            [Obsolete("for serilization")]
            public ReturnedRecord()
            {
            }

            public ReturnedRecord(Guid userId,Guid bookId,DateTime returnDate,bool isPostpone, TimeSpan postpone)
            {
                UserId = userId;
                BookId = bookId;
                ReturnDate = returnDate;
                IsPostpone = isPostpone;
                PostponeDate = postpone;
            }

            public Guid UserId { get; private set; }
            public Guid BookId { get; private set; }
            public DateTime ReturnDate { get; private set; }
            public bool IsPostpone { get; private set; }
            public TimeSpan PostponeDate { get; private set; }
        }

    }
}
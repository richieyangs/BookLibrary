﻿using System;
using System.Collections.Generic;
using BookLibrary.Core.Events;

namespace BookLibrary.Events.BookLibraryProcess
{
    public partial class BookLibraryProcessEvent
    {
        public class BookLibraryProcessCreatedEvent : IEntityCreatedEvent, IBookLibraryEvent
        {
            [Obsolete("for serilization")]
            public BookLibraryProcessCreatedEvent()
            {
            }

            public BookLibraryProcessCreatedEvent(Guid bookLibraryProcessId,Guid userId,DateTime borrowedDate)
            {
                BookLibraryProcessId = bookLibraryProcessId;
                UserId = userId;
                BorrowedDate = borrowedDate;
            }

            public Guid BookLibraryProcessId { get; private set; }
            public Guid UserId { get; private set; }
            public DateTime BorrowedDate { get; private set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using BookLibrary.Core.Events;

namespace BookLibrary.Events.BookLibraryProcess
{
    public partial class BookLibraryProcessEvent
    {
        public class BookLibraryProcessBookBorrowedEvent : IEntityCreatedEvent, IBookLibraryEvent
        {
            [Obsolete("for serilization")]
            public BookLibraryProcessBookBorrowedEvent() { }
            

            public BookLibraryProcessBookBorrowedEvent(Guid bookLibraryProcessId,BorrowedRecord borrowedRecord)
            {
                BookLibraryProcessId = bookLibraryProcessId;
                BorrowedRecord = borrowedRecord;
            }

            public Guid BookLibraryProcessId { get; private set; }
            public BorrowedRecord BorrowedRecord { get; private set; }

        }
    }
}
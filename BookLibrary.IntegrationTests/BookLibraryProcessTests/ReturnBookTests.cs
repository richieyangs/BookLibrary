﻿using System.Linq;
using BookLibrary.IntegrationTests.BookLibraryProcessTests.Scenarios;
using FluentAssertions;
using Xunit;

namespace BookLibrary.IntegrationTests.BookLibraryProcessTests
{
    [Collection("IntegrationTests")]
    public class ReturnBookTests:TestBase
    {
        [Fact]
        public void When_ReturnBook_Should_CreateReturnBookRecord()
        {
            //Arrange
            var returnBookScenario=new ReturnBookScenario(Container);

            //Act
            returnBookScenario.Execute();

            //Assert
            var process = BookLibraryProcessQueryReader.Get(returnBookScenario.BookLibraryProcessId);
            process.BookReturnedRecords.Count.Should().Be(1);
        }

        [Fact]
        public void After_ReturnBook_Should_MarkBorrowedRecordAsReturned()
        {
            //Arrange
            var returnBookScenario = new ReturnBookScenario(Container);

            //Act
            returnBookScenario.Execute();

            //Assert
            var process = BookLibraryProcessQueryReader.Get(returnBookScenario.BookLibraryProcessId);
            process.BookBorrowedRecords.Single(x => x.Book.BookId == returnBookScenario.BookId)
                .IsReturn.Should()
                .BeTrue();
        }

    }
}
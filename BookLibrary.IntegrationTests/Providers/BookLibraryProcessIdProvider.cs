﻿using System;
using System.ComponentModel;
using BookLibrary.QueryModelReader.Contracts;
using Castle.Windsor;

namespace BookLibrary.IntegrationTests.Providers
{
    public class BookLibraryProcessIdProvider
    {
        private readonly IBookLibraryProcessQueryReader _bookLibraryProcessQueryReader;

        public BookLibraryProcessIdProvider(IBookLibraryProcessQueryReader bookLibraryProcessQueryReader)
        {
            _bookLibraryProcessQueryReader = bookLibraryProcessQueryReader;
        }

        public Guid? GetBookLibraryProcessId(Guid userId)
        {
            var bookLibraryProcessId = _bookLibraryProcessQueryReader.GetBookLibraryProcessId(userId);
            return bookLibraryProcessId;
        }
    }
}
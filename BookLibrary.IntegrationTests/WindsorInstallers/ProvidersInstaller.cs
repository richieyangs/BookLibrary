﻿using BookLibrary.IntegrationTests.Providers;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace BookLibrary.IntegrationTests.WindsorInstallers
{
    public class ProvidersInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<BookLibraryProcessIdProvider>().LifestyleScoped());
        }
    }
}
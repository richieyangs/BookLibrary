﻿using System;
using System.Collections.Generic;
using BookLibrary.Domain.BorrowedProcess;

namespace BookLibrary.DomainService.Contracts
{
    public interface IBookLibraryProcessDomainService:IDomainService
    {
        BookLibraryProcess GetBookLibraryProcess(Guid bookLibraryProcessId);

        void BorrowBooks(Guid bookLibraryProcessId, Guid bookId, TimeSpan borrowedInterval);

        void BorrowBooksOnFirstTime(Guid userId, Guid bookId, TimeSpan borrowedInterval);

        void ReturnBook(Guid bookLibraryProcessId, Guid bookId);
    }
}
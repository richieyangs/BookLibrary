﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookLibrary.Events.BookLibraryProcess;

namespace BookLibrary.QueryModel
{
    public class BookLibraryProcessModel
    {
        public BookLibraryProcessModel()
        {
            BookBorrowedRecords = new List<BorrowedRecord>();
            BookReturnedRecords = new List<ReturnedRecord>();
        }

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime BorrowDate { get; set; }
        public  List<BorrowedRecord> BookBorrowedRecords { get; set; }
        public  List<ReturnedRecord> BookReturnedRecords { get; set; }

        public class Book
        {
            [Obsolete("for serilization")]
            public Book(){}

            public Book(Guid bookId, string bookName)
            {
                BookId = bookId;
                BookName = bookName;
            }

            public Guid BookId { get; private set; }
            public string BookName { get; private set; }
        }

        public class BorrowedRecord
        {
            [Obsolete("for serilization")]
            public BorrowedRecord() { }

            public BorrowedRecord(BookLibraryProcessEvent.BorrowedRecord borrowedRecord)
            {
                UserId = borrowedRecord.UserId;
                Book=new Book(borrowedRecord.Book.BookId,borrowedRecord.Book.BookName);
                BorrowedDate = borrowedRecord.BorrowedDate;
                BorrowedInterval = borrowedRecord.BorrowedInterval;
            }
           
            public Guid UserId { get;  set; }
            public Book Book { get;  set; }
            public DateTime BorrowedDate { get;  set; }
            public TimeSpan BorrowedInterval { get;  set; }
            public bool IsReturn { get;  set; }
        }

        public class ReturnedRecord
        {
            [Obsolete("for serilization")]
            public ReturnedRecord() { }

            public ReturnedRecord(BookLibraryProcessEvent.ReturnedRecord returnedRecord)
            {
                UserId = returnedRecord.UserId;
                BookId = returnedRecord.BookId;
                ReturnDate = returnedRecord.ReturnDate;
                IsPostpone = returnedRecord.IsPostpone;
                PostponeDate = returnedRecord.PostponeDate;
            }

            public Guid UserId { get;  set; }
            public Guid BookId { get;  set; }
            public DateTime ReturnDate { get;  set; }
            public bool IsPostpone { get;  set; }
            public TimeSpan PostponeDate { get;  set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BookLibrary.QueryModel
{
    public class UserAndLibraryProcessMapper
    {
        public static string Index()
        {
            return "urn:user>bookLibraryProcess:";
        }

        [Obsolete("for serialization")]
        public UserAndLibraryProcessMapper() { }

        public UserAndLibraryProcessMapper(Guid bookLibraryProcessId,Guid userId)
        {
            BookLibraryProcessId = bookLibraryProcessId;
            UserId = userId;
        }
        public Guid BookLibraryProcessId { get; set; }
        public Guid UserId { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace BookLibrary.ApplicationService.Contracts
{
    public interface IBookLibraryProcessService:IApplicationService
    {
        void BorrowBooks(Guid? bookLibraryProcessId, Guid userId,Guid bookIds, TimeSpan borrowedInterval);

        void ReturnBook(Guid bookLibraryProcessId, Guid bookId);
    }
}
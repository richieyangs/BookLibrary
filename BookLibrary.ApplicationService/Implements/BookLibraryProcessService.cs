﻿using System;
using System.Collections.Generic;
using BookLibrary.ApplicationService.Contracts;

namespace BookLibrary.ApplicationService.Implements
{
    public class BookLibraryProcessService:IBookLibraryProcessService
    {
        private readonly DomainService.Contracts.IBookLibraryProcessDomainService _bookLibraryProcessDomainService;

        public BookLibraryProcessService(DomainService.Contracts.IBookLibraryProcessDomainService bookLibraryProcessDomainService)
        {
            _bookLibraryProcessDomainService = bookLibraryProcessDomainService;
        }

        public void BorrowBooks(Guid userId, Guid bookId, TimeSpan borrowInterval)
        {
            _bookLibraryProcessDomainService.BorrowBooks(userId,bookId,borrowInterval);
        }

        public void BorrowBooks(Guid? bookLibraryProcessId, Guid userId, Guid bookId, TimeSpan borrowedInterval)
        {
            if (bookLibraryProcessId.HasValue)
            {
                _bookLibraryProcessDomainService.BorrowBooks(bookLibraryProcessId.Value,bookId,borrowedInterval);
            }
            else
            {
                _bookLibraryProcessDomainService.BorrowBooksOnFirstTime(userId,bookId,borrowedInterval);
            }
        }

        public void ReturnBook(Guid bookLibraryProcessId, Guid bookId)
        {
            _bookLibraryProcessDomainService.ReturnBook(bookLibraryProcessId,bookId);
        }
    }
}